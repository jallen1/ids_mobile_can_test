﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using IDS.Core;
using IDS.Core.Events;
using IDS.Core.IDS_CAN;
using IDS.Core.Tasks;
using IDS.Portable.CAN;
using IDS.Portable.CAN.Com;
using IDS.Portable.Common;
using IDS.Portable.Platform;
using IDS.Portable.Platform.Net.BLE;
using Xamarin.Forms;
using IBleDevice = Plugin.BLE.Abstractions.Contracts.IDevice;

namespace ids_mobile_can_test
{
    public interface IConnection
    {
        Task<IAdapter> MakeAdapterAsync(CancellationToken ct);
    }

    public class BleConnection : IConnection
    {
        private readonly string _name;
        private readonly string _password;

        public BleConnection(string name, string password)
        {
            _name = name;
            _password = password;
        }

        public async Task<IAdapter> MakeAdapterAsync(CancellationToken ct)
        {
            (var gateway, var version) = await ScanForBleDevice(_name, ct);
            return CanAdapterFactory.CreateBleAdapter(CanDeviceInfo.Instance, gateway.Id, _name, _password, version, verbose: true);
        }

        private async Task<(IBleDevice gateway, BleGatewayInfo.GatewayVersion version)> ScanForBleDevice(string name, CancellationToken ct)
        {
            var tcs = new TaskCompletionSource<(IBleDevice gateway, BleGatewayInfo.GatewayVersion version)>(TaskCreationOptions.RunContinuationsAsynchronously);

            using (var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(ct))
            {
                using (linkedCts.Token.Register(() => tcs.TrySetCanceled()))
                {
                    try
                    {
                        await BleGateway.Instance.ScanForBleGateways((gateway, version) =>
                        {
                            if (string.Equals(gateway.Name, name, StringComparison.InvariantCultureIgnoreCase))
                                tcs.TrySetResult((gateway: gateway, version: version));
                        }, linkedCts.Token);
                    }
                    catch (Exception e)
                    {
                        tcs.TrySetException(e);
                    }
                    return await tcs.Task;
                }
            }
        }
    }

    public class WifiConnection : IConnection
    {
        private readonly string _ssid;
        private readonly string _password;

        public WifiConnection(string ssid, string password)
        {
            _ssid = ssid;
            _password = password;
        }

        public async Task<IAdapter> MakeAdapterAsync(CancellationToken ct)
        {
            var connected = await ScanAndConnectToWifi(_ssid, _password, ct);
            return connected
                ? CanAdapterFactory.CreateTcpSocketAdapter(CanDeviceInfo.Instance, verbose: false)
                : null;
        }

        private async Task<bool> ScanAndConnectToWifi(string ssid, string password, CancellationToken ct)
        {
            var res = await MyRvWifiNetworkManager.Instance.ConnectAsync(_ssid, password, ct);
            return res;
        }
    }

    public class Connection : BackgroundOperationService<Connection>
    {
        private readonly object _syncConnection = new object();
        private IConnection _connection;
        private bool _running = false;
        public IAdapter _gateway;
        private SubscriptionManager _subscritpionManager;
        private readonly Stopwatch _timerConnecting = new Stopwatch();
        private readonly Stopwatch _timerConnected = new Stopwatch();
        private readonly Stopwatch _timerOnline = new Stopwatch();
        
        public string OnOpen { get; } = "OnOpen";
        public string OnClose { get; } = "OnClose";
        public string OnRead { get; } = "OnRead";
        public string OnWrite { get; } = "OnWrite";
        public string OnDeviceOnline { get; } = "OnDeviceOnline";
        public string OnDeviceRename { get; } = "OnDeviceRename";
        public string OnDeviceOffline { get; } = "OnDeviceOffline";
        public string OnAdapterOnline { get; } = "OnAdapterOnline";
        public string OnAdapterOffline { get; } = "OnAdapterOffline";

        public bool IsConnecting => _running && !IsConnected;
        public double SecConnecting => IsConnecting ? _timerConnecting.Elapsed.TotalSeconds : 0;
        public int ConnectAttempts { get; private set; }

        public bool IsConnected => _gateway?.IsConnected ?? false;
        public double SecConnected => _timerConnected.Elapsed.TotalSeconds;

        private bool _isOnline = false;
        public bool IsOnline => IsConnected && _isOnline;
        public double SecOnline => IsConnected && IsOnline ? _timerOnline.Elapsed.TotalSeconds : 0;
        public int DevicesOnline => IsConnected ? _gateway.Devices.NumDevicesDetectedOnNetwork : 0;
        
        public long RxMsgCnt => IsConnected ? _gateway.MessagesReceived : 0;
        public double RxMsgsPerSec => IsConnected && _timerConnected.Elapsed.TotalSeconds > 0 ? _gateway.MessagesReceived / _timerConnected.Elapsed.TotalSeconds : 0;
        public double LastMsgRxMs => IsConnected ? (int) (_gateway.TimeSinceLastMessageRx_sec * 1000) : 0;

        public long TxMsgCnt => IsConnected ? _gateway.MessagesSent : 0;
        public double TxMsgsPerSec => IsConnected && _timerConnected.Elapsed.TotalSeconds > 0 ? _gateway.MessagesSent / _timerConnected.Elapsed.TotalSeconds : 0;
        public double LastMsgTxMs => IsConnected ? (int)(_gateway.TimeSinceLastMessageTx_sec * 1000) : 0;

        public void SetAndStartConnection(IConnection connection)
        {
            lock (_syncConnection)
            {
                Stop();
                _connection = connection;
                Start();
            }
        }

        public void SetConnection(IConnection connection)
        {
            lock (_syncConnection)
            {
                Stop();
                _connection = connection;
            }
        }

        protected string Tag => GetType().Name;

        protected Connection() { }
        
        protected override async Task BackgroundOperationAsync(CancellationToken ct)
        {
            TaggedLog.Debug(Tag, $"{nameof(BackgroundOperationAsync)} - BEGIN");
            _running = true;

            while (!ct.IsCancellationRequested)
            {
                try
                {
                    if (_connection != null)
                        TaggedLog.Debug(Tag, $"IsConnected = {IsConnected}");

                    if (!IsConnected && _connection != null)
                    {
                        _timerConnecting.Start();
                        ConnectAttempts++;
                        
                        _gateway = await _connection.MakeAdapterAsync(ct);
                        _subscritpionManager = new SubscriptionManager();
                        _gateway.Events.Subscribe<CommunicationsAdapter.AdapterOpenedEvent>(OnPhysicalNetworkOpened, SubscriptionType.Strong, _subscritpionManager);
                        _gateway.Events.Subscribe<CommunicationsAdapter.AdapterClosedEvent>(OnPhysicalNetworkClosed, SubscriptionType.Strong, _subscritpionManager);
                        _gateway.Events.Subscribe<LocalDeviceOnlineEvent>(OnLocalDeviceOnline, SubscriptionType.Strong, _subscritpionManager);
                        _gateway.Events.Subscribe<LocalDeviceOfflineEvent>(OnLocalDeviceOffline, SubscriptionType.Strong, _subscritpionManager);
                        _gateway.Events.Subscribe<CommunicationsAdapter.AdapterTxEvent<MessageBuffer>>(OnPhysicalNetworkTransmitted, SubscriptionType.Strong, _subscritpionManager);
                        _gateway.Events.Subscribe<AdapterRxEvent>(OnPhysicalNetworkReceived, SubscriptionType.Strong, _subscritpionManager);
                        _gateway.Events.Subscribe<RemoteDeviceOnlineEvent>(OnRemoteDeviceOnline, SubscriptionType.Strong, _subscritpionManager);
                        _gateway.Events.Subscribe<RemoteDeviceOfflineEvent>(OnRemoteDeviceOffline, SubscriptionType.Strong, _subscritpionManager);

                        await _gateway.OpenAsync(new AsyncOperation(20, ct));
                    }
                }
                catch (Exception e)
                {
                    TaggedLog.Error(Tag, $"{nameof(BackgroundOperationAsync)}: {e.GetType()} {e.Message}\n{e.StackTrace}"); 
                }
                finally
                {
                    if (!IsConnected)
                        await DisconnectAsync();
                    else
                    {
                        _timerConnecting.Stop();
                        _timerConnecting.Reset();
                        ConnectAttempts = 0;
                    }
                }
                
                await Task.Delay(5000, ct).TryAwaitAsync();
            }

            _running = false;
            TaggedLog.Debug(Tag, $"{nameof(BackgroundOperationAsync)} - END");
        }

        public override async void Stop()
        {
            base.Stop();
            await DisconnectAsync();
        }

        private async Task DisconnectAsync()
        {
            TaggedLog.Debug(Tag, $"{nameof(DisconnectAsync)} - BEGIN");

            TaggedLog.Debug(Tag, "_subscritpionManager.CancelAllSubscriptions");
            try { _subscritpionManager?.CancelAllSubscriptions(); }
            catch (Exception e0)  { TaggedLog.Information(Tag, $"{nameof(DisconnectAsync)} - _subscritpionManager.CancelAllSubscriptions - {e0.GetType()}: {e0.Message}"); }

            TaggedLog.Debug(Tag, "_subscritpionManager.Dispose");
            try { _subscritpionManager?.Dispose(); }
            catch (Exception e1) { TaggedLog.Information(Tag, $"{nameof(DisconnectAsync)} - _subscritpionManager.Dispose - {e1.GetType()}: {e1.Message}"); }

            TaggedLog.Debug(Tag, "_gateway.CloseAsync");
            try { if (_gateway != null) await _gateway.CloseAsync(new AsyncOperation(1, CancellationToken.None)); }
            catch (Exception e2) { TaggedLog.Information(Tag, $"{nameof(DisconnectAsync)} - _gateway.CloseAsync - {e2.GetType()}: {e2.Message}"); }

            TaggedLog.Debug(Tag, "_gateway.Dispose - BEGIN");
            try { _gateway?.Dispose(); }
            catch (Exception e3) { TaggedLog.Information(Tag, $"{nameof(DisconnectAsync)} - _gateway.Dispose - {e3.GetType()}: {e3.Message}"); }
            TaggedLog.Debug(Tag, "_gateway.Dispose - END");

            _timerConnected.Reset();
            _timerOnline.Reset();

            TaggedLog.Debug(Tag, $"{nameof(DisconnectAsync)} - END");
        }

        private void OnPhysicalNetworkOpened(CommunicationsAdapter.AdapterOpenedEvent obj)
        {
            TaggedLog.Debug(Tag, nameof(OnPhysicalNetworkOpened));
            _timerConnected.Restart();
            MessagingCenter.Send(this, OnOpen);
        }

        private void OnPhysicalNetworkClosed(CommunicationsAdapter.AdapterClosedEvent obj)
        {
            TaggedLog.Debug(Tag, nameof(OnPhysicalNetworkClosed));
            _timerConnected.Reset();
            MessagingCenter.Send(this, OnClose);
        }

        private void OnLocalDeviceOnline(LocalDeviceOnlineEvent obj)
        {
            _isOnline = true;
            _timerOnline.Restart();
            MessagingCenter.Send(this, OnDeviceOnline, obj.Device);
        }

        private void OnLocalDeviceOffline(LocalDeviceOfflineEvent obj)
        {
            _isOnline = false;
            _timerOnline.Reset();
            MessagingCenter.Send(this, OnDeviceOffline, obj.Device);
        }

        private void OnPhysicalNetworkTransmitted(CommunicationsAdapter.AdapterTxEvent<MessageBuffer> obj)
            => MessagingCenter.Send(this, OnWrite, obj.Message);

        private void OnPhysicalNetworkReceived(AdapterRxEvent obj)
            => MessagingCenter.Send(this, OnRead, obj.Message);

        private void OnRemoteDeviceOnline(RemoteDeviceOnlineEvent obj)
        {
            //obj.Device.Events.Subscribe<DeviceIDChangedEvent>(OnDeviceIdChanged, SubscriptionType.Strong, _subscritpionManager);
            MessagingCenter.Send(this, OnDeviceOnline, obj.Device);
        }

        private void OnDeviceIdChanged(DeviceIDChangedEvent obj)
            => MessagingCenter.Send(this, OnDeviceRename, obj.Device);

        private void OnRemoteDeviceOffline(RemoteDeviceOfflineEvent obj)
            => MessagingCenter.Send(this, OnDeviceOffline, obj.Device);
    }
}
