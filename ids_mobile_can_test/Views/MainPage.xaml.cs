﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ids_mobile_can_test.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}