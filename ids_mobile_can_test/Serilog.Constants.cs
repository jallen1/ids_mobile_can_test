﻿using System;
using System.IO;
using Serilog.Core;
using Serilog.Events;

namespace ids_mobile_can_test
{
    public static class SerilogConstants
    {
#if DEBUG
        public static LogEventLevel LogDefaultEventLevel = LogEventLevel.Debug;
#else
            public static LogEventLevel LogDefaultEventLevel = LogEventLevel.Information;
#endif

        public static readonly LoggingLevelSwitch LogLevelCurrent = new LoggingLevelSwitch(LogDefaultEventLevel);
        public static readonly string LogFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        public const string LogFileNameBase = "OneControl";
        public const string LogFileNameExtension = "log";
        public static readonly string LogFileNameFull = Path.Combine(LogFolder, $"{LogFileNameBase}.{LogFileNameExtension}");  // Note: The actual log filename will vary based on date/time stamp.
        public const long LogFileSizeLimitBytes = 1024 * 1024 * 10;
        public const int LogFileRetainedCountLimit = 7;
        public static readonly string LogFileOutputTemplate = $"{{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz}} [{{Level:u3}}:{{{Constants.SourceContextPropertyName}:l}}] {{Message:l}}{{NewLine}}{{Exception}}";
        public const string LogcatConsoleOutputTemplate = "{Message:l}{NewLine:l}{Exception:l}"; // Don't include tag (aka SourceContext) in output because logcat already does
        public static readonly string LogConsoleOutputTemplate = $"[{{Level}}:{{{Constants.SourceContextPropertyName}:l}}] {{Message:l}}{{NewLine:l}}{{Exception:l}}";
    }
}
