﻿using IDS.Core.IDS_CAN;
using IDS.Portable.CAN;
using IDS.Portable.Common;
using IDS.Portable.Platform;

namespace ids_mobile_can_test
{
    public class CanDeviceInfo : Singleton<CanDeviceInfo>, ICanDeviceInfo
    {
        private CanDeviceInfo()
        {
            DeviceId = new DEVICE_ID(ProductId, 0, DeviceType, 0, FunctionName, 0, 0); // Core v2.6 requires that a value be passed for device capabilities
        }

        public string PartNumber => DeviceInfo.Instance.Model;

        public DEVICE_ID DeviceId { get; private set; }

        public DEVICE_TYPE DeviceType => DeviceInfo.Instance.OS == DeviceOS.Android
            ? DEVICE_TYPE.ANDROID_MOBILE_DEVICE
            : (DeviceInfo.Instance.OS == DeviceOS.iOS
                ? DEVICE_TYPE.IOS_MOBILE_DEVICE
                : DEVICE_TYPE.UNKNOWN);

        public FUNCTION_NAME FunctionName => FUNCTION_NAME.DIAGNOSTIC_TOOL;

        public PRODUCT_ID ProductId => DeviceInfo.Instance.OS == DeviceOS.Android
            ? PRODUCT_ID.ANDROID_DEVICE
            : (DeviceInfo.Instance.OS == DeviceOS.iOS
                ? PRODUCT_ID.IOS_DEVICE
                : PRODUCT_ID.UNKNOWN);
    }
}
