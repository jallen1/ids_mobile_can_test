﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using ids.portable.common.Extensions;
using IDS.Core.IDS_CAN;
using IDS.Portable.CAN;
using IDS.Portable.CAN.Com;
using IDS.Portable.Common;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ids_mobile_can_test.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
#if true
        private const string type = "BLE";
        
        private const string name = "LCIRemoteSRKNTVbRZ";
        private const string password = "235605";
        // private const string name = JARED_BETA_REVE";
        // private const string password = "111111";
#else
        private const string type = "WiFi";
        private const string name = "Jared_MyRV";
        private const string password = "1234567890";
#endif

        private const string _tag = nameof(MainViewModel);

        private readonly Connection _connection;

        private const string ConnectionType_BLE = "BLE";
        private const string ConnectionType_WiFi = "WiFi";

        public List<string> ConnectionTypes { get; } = new List<string>()
        {
            ConnectionType_BLE,
            ConnectionType_WiFi
        };

        private string _connectionType = type;

        public string ConnectionType
        {
            get => _connectionType;
            set => SetProperty(ref _connectionType, value);
        }

        private string _name = name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value, Name);
        }

        private string _password = password;

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value, Password);
        }

        public bool CanConnect => !_connection.IsConnecting && !_connection.IsConnected;

        public string StartStopText => CanConnect
            ? "CONNECT"
            : "DISCONNECT";

        private ICommand _onConnectDisconnectClicked;
        public ICommand OnConnectDisconnectClicked => _onConnectDisconnectClicked ?? (_onConnectDisconnectClicked = new Command(DoConnectDisconnect));

        private void DoConnectDisconnect()
        {
            if (CanConnect)
            {
                if (string.Equals(ConnectionType, ConnectionType_BLE))
                    _connection.SetAndStartConnection(new BleConnection(Name, Password));

                if (string.Equals(ConnectionType, ConnectionType_WiFi))
                    _connection.SetAndStartConnection(new WifiConnection(Name, Password));
            }
            else
                _connection.Stop();
        }

        public MainViewModel()
        {
            Title = "IDS CAN Test";

            _connection = Connection.Instance;

            foreach (var asm in GetType().Assembly.GetReferencedAssemblies())
            {
                if (string.Equals(asm.Name, "IDS.Core", StringComparison.OrdinalIgnoreCase))
                    CoreVersion = asm.Version.ToString();
                if (string.Equals(asm.Name, "IDS.Core.IDS_CAN", StringComparison.OrdinalIgnoreCase))
                    CanVersion = asm.Version.ToString();
                if (string.Equals(asm.Name, "IDS.Core.IDS_CAN.Descriptors", StringComparison.OrdinalIgnoreCase))
                    DescriptorsVersion = asm.Version.ToString();
            }

            //// MessagingCenter.Subscribe<Connection, IRemoteDevice>(this, _connection.OnDeviceOnline, (c, d) =>
            ////     TaggedLog.Information("DEVICE INFO", $"ONLINE, {d.MAC}, {d.Product.Name}, {d.DeviceType.Name}, {d.DeviceInstance}, {d.FunctionName.Name}, {d.FunctionInstance}, {d.Address}"));
            //// 
            //// MessagingCenter.Subscribe<Connection, IRemoteDevice>(this, _connection.OnDeviceRename, (c, d) =>
            ////     TaggedLog.Information("DEVICE INFO", $"RENAME, {d.MAC}, {d.Product.Name}, {d.DeviceType.Name}, {d.DeviceInstance}, {d.FunctionName.Name}, {d.FunctionInstance}, {d.Address}"));
            //// 
            //// MessagingCenter.Subscribe<Connection, IRemoteDevice>(this, _connection.OnDeviceOffline, (c, d) =>
            ////     TaggedLog.Information("DEVICE INFO" , $"OFFLINE, {d.MAC}, {d.Product.Name}, {d.DeviceType.Name}, {d.DeviceInstance}, {d.FunctionName.Name}, {d.FunctionInstance}, {d.Address}"));

            Task.Run(() => OneSecondTask());
        }

        private string _connectionStatus;

        public string ConnectionStatus
        {
            get => _connectionStatus;
            private set
            {
                if (string.Equals(value, _connectionStatus)) return;
                _connectionStatus = value;
                OnPropertyChanged(nameof(ConnectionStatus));
                OnPropertyChanged(nameof(CanConnect));
                OnPropertyChanged(nameof(StartStopText));
            }
        }

        public string CoreVersion { get; }
        public string CoreBuildDate { get; } = IDS.Core.BuildInfo.DateTime.ToString("MM/dd/yyyy hh:mm tt");
        public string CanVersion { get; }
        public string CanBuildDate { get; } = IDS.Core.IDS_CAN.BuildInfo.DateTime.ToString("MM/dd/yyyy hh:mm tt");
        public string DescriptorsVersion { get; }
        public string DescriptorsBuildDate { get; } = IDS.Core.IDS_CAN.Descriptors.BuildInfo.DateTime.ToString("MM/dd/yyyy hh:mm tt");

        private bool _isConnected = false;

        public bool IsConnected
        {
            get => _isConnected;
            private set
            {
                if (value == _isConnected) return;
                _isConnected = value;
                OnPropertyChanged(nameof(IsConnected));
                OnPropertyChanged(nameof(CanConnect));
                OnPropertyChanged(nameof(StartStopText));
            }
        }

        private double _secConnected = 0;

        public double SecConnected
        {
            get => _secConnected;
            private set => SetProperty(ref _secConnected, value);
        }

        private bool _isOnline = false;

        public bool IsOnline
        {
            get => _isOnline;
            private set => SetProperty(ref _isOnline, value);
        }

        private double _secOnline = 0;

        public double SecOnline
        {
            get => _secOnline;
            private set => SetProperty(ref _secOnline, value);
        }

        private int _devicesOnline = 0;

        public int DevicesOnline
        {
            get => _devicesOnline;
            private set => SetProperty(ref _devicesOnline, value);
        }

        private long _txMsgCnt = 0;

        public long TxMsgCnt
        {
            get => _txMsgCnt;
            private set => SetProperty(ref _txMsgCnt, value);
        }

        private double _txMsgPerSec = 0;

        public double TxMsgPerSec
        {
            get => _txMsgPerSec;
            private set => SetProperty(ref _txMsgPerSec, value);
        }

        private long _rxMsgCnt = 0;

        public long RxMsgCnt
        {
            get => _rxMsgCnt;
            private set => SetProperty(ref _rxMsgCnt, value);
        }

        private double _rxMsgPerSec = 0;

        public double RxMsgPerSec
        {
            get => _rxMsgPerSec;
            private set => SetProperty(ref _rxMsgPerSec, value);
        }

        private long _physicalAdapterRaiseRxAvg = 0;
        public long PhysicalAdapter_RaiseRXAvg
        {
            get => _physicalAdapterRaiseRxAvg;
            private set => SetProperty(ref _physicalAdapterRaiseRxAvg, value);
        }

        private int _physicalAdapter_RaiseRXCnt_100;
        public int PhysicalAdapter_RaiseRXCnt_100
        {
            get => _physicalAdapter_RaiseRXCnt_100;
            private set => SetProperty(ref _physicalAdapter_RaiseRXCnt_100, value);
        }

        private int _physicalAdapter_RaiseRXCnt_200;
        public int PhysicalAdapter_RaiseRXCnt_200
        {
            get => _physicalAdapter_RaiseRXCnt_200;
            private set => SetProperty(ref _physicalAdapter_RaiseRXCnt_200, value);
        }

        private int _physicalAdapter_RaiseRXCnt_300;
        public int PhysicalAdapter_RaiseRXCnt_300
        {
            get => _physicalAdapter_RaiseRXCnt_300;
            private set => SetProperty(ref _physicalAdapter_RaiseRXCnt_300, value);
        }

        private int _physicalAdapter_RaiseRXCnt_400;
        public int PhysicalAdapter_RaiseRXCnt_400
        {
            get => _physicalAdapter_RaiseRXCnt_400;
            private set => SetProperty(ref _physicalAdapter_RaiseRXCnt_400, value);
        }

        private int _physicalAdapter_RaiseRXCnt_500;
        public int PhysicalAdapter_RaiseRXCnt_500
        {
            get => _physicalAdapter_RaiseRXCnt_500;
            private set => SetProperty(ref _physicalAdapter_RaiseRXCnt_500, value);
        }

        private int _physicalAdapter_RaiseRXCnt_1000;
        public int PhysicalAdapter_RaiseRXCnt_1000
        {
            get => _physicalAdapter_RaiseRXCnt_1000;
            private set => SetProperty(ref _physicalAdapter_RaiseRXCnt_1000, value);
        }

        private long _canAdapterRaiseRxAvg = 0;
        public long CanAdapter_RaiseRXAvg
        {
            get => _canAdapterRaiseRxAvg;
            private set => SetProperty(ref _canAdapterRaiseRxAvg, value);
        }

        private int _canAdapter_RaiseRXCnt_100;
        public int CanAdapter_RaiseRXCnt_100
        {
            get => _canAdapter_RaiseRXCnt_100;
            private set => SetProperty(ref _canAdapter_RaiseRXCnt_100, value);
        }

        private int _canAdapter_RaiseRXCnt_200;
        public int CanAdapter_RaiseRXCnt_200
        {
            get => _canAdapter_RaiseRXCnt_200;
            private set => SetProperty(ref _canAdapter_RaiseRXCnt_200, value);
        }

        private int _canAdapter_RaiseRXCnt_300;
        public int CanAdapter_RaiseRXCnt_300
        {
            get => _canAdapter_RaiseRXCnt_300;
            private set => SetProperty(ref _canAdapter_RaiseRXCnt_300, value);
        }

        private int _canAdapter_RaiseRXCnt_400;
        public int CanAdapter_RaiseRXCnt_400
        {
            get => _canAdapter_RaiseRXCnt_400;
            private set => SetProperty(ref _canAdapter_RaiseRXCnt_400, value);
        }

        private int _canAdapter_RaiseRXCnt_500;
        public int CanAdapter_RaiseRXCnt_500
        {
            get => _canAdapter_RaiseRXCnt_500;
            private set => SetProperty(ref _canAdapter_RaiseRXCnt_500, value);
        }

        private int _canAdapter_RaiseRXCnt_1000;
        public int CanAdapter_RaiseRXCnt_1000
        {
            get => _canAdapter_RaiseRXCnt_1000;
            private set => SetProperty(ref _canAdapter_RaiseRXCnt_1000, value);
        }

        private async void OneSecondTask()
        {
            var heartbeat = 0;
            while (true)
            {
                ConnectionStatus = _connection.IsConnected
                    ? "connected"
                    : _connection.IsConnecting
                        ? $"connecting... {_connection.ConnectAttempts}, {_connection.SecConnecting:F0}s"
                        : "not connected";

                IsConnected = Connection.Instance.IsConnected;
                SecConnected = _connection.SecConnected;

                IsOnline = _connection.IsOnline;
                SecOnline = _connection.SecOnline;
                DevicesOnline = _connection.DevicesOnline;

                TxMsgCnt = _connection.TxMsgCnt;
                TxMsgPerSec = _connection.TxMsgsPerSec;

                RxMsgCnt = _connection.RxMsgCnt;
                RxMsgPerSec = _connection.RxMsgsPerSec;

#if true
                var phyAvg = SynchronizedCommunicationsAdapter.RxDurationMetrics.Metrics.FirstOrDefault()?.Average;
                PhysicalAdapter_RaiseRXAvg = phyAvg?.ElapsedMicroseconds() ?? 0;
                
                PhysicalAdapter_RaiseRXCnt_100 = phyAvg != null
                    ? SynchronizedCommunicationsAdapter.RxDurationMetrics.Metrics[1].Count
                    : 0;
                
                PhysicalAdapter_RaiseRXCnt_200 = phyAvg != null
                    ? SynchronizedCommunicationsAdapter.RxDurationMetrics.Metrics[2].Count
                    : 0;
                
                PhysicalAdapter_RaiseRXCnt_300 = phyAvg != null
                    ? SynchronizedCommunicationsAdapter.RxDurationMetrics.Metrics[3].Count
                    : 0;
                
                PhysicalAdapter_RaiseRXCnt_400 = phyAvg != null
                    ? SynchronizedCommunicationsAdapter.RxDurationMetrics.Metrics[4].Count
                    : 0;
                
                PhysicalAdapter_RaiseRXCnt_500 = phyAvg != null
                    ? SynchronizedCommunicationsAdapter.RxDurationMetrics.Metrics[5].Count
                    : 0;
                
                PhysicalAdapter_RaiseRXCnt_1000 = phyAvg != null
                    ? SynchronizedCommunicationsAdapter.RxDurationMetrics.Metrics[6].Count
                    : 0;
                
                var canAvg = CanAdapter.RaiseRXMetrics.Metrics.FirstOrDefault()?.Average;
                CanAdapter_RaiseRXAvg = canAvg?.ElapsedMicroseconds() ?? 0;
                
                CanAdapter_RaiseRXCnt_100 = canAvg != null
                    ? CanAdapter.RaiseRXMetrics.Metrics[1].Count
                    : 0;
                
                CanAdapter_RaiseRXCnt_200 = canAvg != null
                    ? CanAdapter.RaiseRXMetrics.Metrics[2].Count
                    : 0;
                
                CanAdapter_RaiseRXCnt_300 = canAvg != null
                    ? CanAdapter.RaiseRXMetrics.Metrics[3].Count
                    : 0;
                
                CanAdapter_RaiseRXCnt_400 = canAvg != null
                    ? CanAdapter.RaiseRXMetrics.Metrics[4].Count
                    : 0;
                
                CanAdapter_RaiseRXCnt_500 = canAvg != null
                    ? CanAdapter.RaiseRXMetrics.Metrics[5].Count
                    : 0;
                
                CanAdapter_RaiseRXCnt_1000 = canAvg != null
                    ? CanAdapter.RaiseRXMetrics.Metrics[6].Count
                    : 0;
#endif

#if true
                heartbeat = (heartbeat + 1) % 20;
                if (heartbeat == 0)
                {
                    TaggedLog.Information(_tag, $"{nameof(OneSecondTask)}: heartbeat");


                    if (_connection?._gateway?.Devices != null)
                    {
                        TaggedLog.Debug("DEVICE", "----- START -----");
                        foreach (var device in _connection?._gateway?.Devices)
                        {
                            TaggedLog.Debug("DEVICE", $"{device.Address.Value:X2}, {device.IsOnline}, {device.FunctionName}, {device.DeviceInstance}");
                        }
                        TaggedLog.Debug("DEVICE", "----- FINISH -----");
                    }
                }
#endif

#if true
                await Task.Delay(canAvg != null
                    ? CanAdapter.RaiseRXMetrics.Metrics[0].SamplingWindow
                    : TimeSpan.FromMilliseconds(500)
                     , CancellationToken.None).ConfigureAwait(false);
#else
                await Task.Delay(1000, CancellationToken.None).ConfigureAwait(false);
#endif
            }
        }
    }
}