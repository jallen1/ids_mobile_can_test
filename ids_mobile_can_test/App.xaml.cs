﻿using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ids_mobile_can_test.Views;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ids_mobile_can_test
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
            => CheckPermission();

        protected override void OnSleep()
            => Connection.Instance.Stop();

        protected override void OnResume()
            => CheckPermission();

        private void CheckPermission()
        {
            CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location).ContinueWith(checkPermissionTask =>
            {
                if (!checkPermissionTask.IsCompleted)
                {
                    CrossPermissions.Current.OpenAppSettings();
                    return;
                }

                if (checkPermissionTask.Result == PermissionStatus.Granted)
                    return;

                CrossPermissions.Current.RequestPermissionsAsync(Permission.Location).ContinueWith(requestPermissionTask =>
                {
                    if (requestPermissionTask.IsCompleted &&
                        requestPermissionTask.Result.TryGetValue(Permission.Location, out var permissionStatus) &&
                        permissionStatus == PermissionStatus.Granted)
                        return;
                    CrossPermissions.Current.OpenAppSettings();
                }, CancellationToken.None);
            }, CancellationToken.None);
        }
    }
}
