@echo off
rem This is used to delete a local branch.  NOTE: you have to supply a branch name and
rem a -d argument to delete the local branch.  The branch being deleted can't be your currently
rem checked out branch!
rem

if "%1"=="" goto help
if not "%2"=="-d" goto help

echo Delete Local Branch "%1"
git branch -d %1
git submodule foreach --recursive "git branch -d %1 || true"

goto exit

:error
echo Stopped because of error
goto exit

:help
echo Missing branch name or -d argument
goto exit

:exit