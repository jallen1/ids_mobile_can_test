#!/bin/bash
set +v

# Delete all obj and bin directories
find . -type d -name "obj" -exec rm -rv {} \;
find . -type d -name "bin" -exec rm -rv {} \;
