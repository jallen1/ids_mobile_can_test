@echo off
rem This is used to make sure all local repos at set to the same branch
rem
rem Checks out the main repo and all submodules on the given branch (LOCAL ONLY)

if "%1"=="" goto help

echo Checkout "%1"
git checkout %1 || goto :error
git submodule update || goto :error
git submodule foreach --recursive "git checkout %1"

goto exit

:error
echo Stopped because of error
goto exit

:help
echo Missing branch name
goto exit

:exit
