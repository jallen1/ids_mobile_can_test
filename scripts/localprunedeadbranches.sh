#!/bin/bash
set +v

# This is used to delete a local branch.  NOTE: you have to supply a branch name and
# a -d argument to delete the local branch.  The branch being deleted can't be your currently
# checked out branch!
if [ $# -eq 0 ];
then
    echo "Missing branch name"
    exit 1
fi

if [ "$2" == "-d" ];
then
    echo Delete Local Branch "$1"
    git branch -d "$1"
    git submodule foreach --recursive "git branch -d $1 || true"    
else
    echo "Missing argument.  -d is required to delete the branch"
    exit 1
fi

