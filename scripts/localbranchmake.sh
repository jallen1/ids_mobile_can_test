#!/bin/bash

set +v
# This is used to make a local branch on all submodules.
#
function error_occurred {
    echo "Stopped because of error"
    exit
}

if [ $# -eq 0 ];
then
    echo "Missing branch name"
    exit 1
fi

echo Make and Switch to Branch "$1"
git checkout -b "$1"
git submodule foreach --recursive git checkout -b "$1"
