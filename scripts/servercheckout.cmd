@echo off
rem This is used to update to the latest branch.  It's useful for
rem getting in sync with our main branches such as beta and master.
rem
rem It checkout and update the local repo to match the branch on the
rem server.  In addition, it will try to checkout the same branch 
rem name for each submodule.
rem

if "%1"=="" goto help

echo Checkout "%1"
git submodule foreach --recursive git fetch --all
git fetch --all || goto :error
git checkout %1 || goto :error
git pull --ff-only || goto :error
git submodule update || goto :error
git submodule foreach --recursive "git checkout %1 && git pull --ff-only"

goto exit

:error
echo Stopped because of error
goto exit

:help
echo Missing branch name
goto exit

:exit