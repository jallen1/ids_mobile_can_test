#!/bin/bash

set +v
# This is used to update to the latest branch.  It's useful for
# getting in sync with our main branches such as beta and master.
#
# It checkout and update the local repo to match the branch on the
# server.  In addition, it will try to checkout the same branch
# name for each submodule.
#
submodules=0

function error_occurred {
    echo "Stopped because of error"
    exit
}

function help {
    echo "Usage: servercheckout.sh [-s] branch"
    echo " -s   Checkout Submodules only"
    exit 1
}

if [ $# -eq 0 ];
then
    help
fi

while getopts ":s:" opt; do
    case $opt in
        s )
            submodules=1
            echo "Checking out submodules only"
        ;;
        \? )
            help;
        ;;
        * )
            help;
        ;;
    esac
done

branch=""

if [ "$submodules" -eq "0" ];
then
    branch="$1"
    git fetch --all || error_occurred
    git checkout "$branch" || error_occurred
    git pull --ff-only || error_occurred
else
    branch="$2"
fi

echo Checking out "$branch"

git submodule foreach --recursive git fetch --all
git submodule update || error_occurred
git submodule foreach --recursive "git checkout $branch && git pull --ff-only"
