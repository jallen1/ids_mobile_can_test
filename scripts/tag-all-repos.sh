#!/bin/bash

set +v
# This is used to update to the latest branch.  It's useful for
# getting in sync with our main branches such as beta and master.
#
# It checkout and update the local repo to match the branch on the
# server.  In addition, it will try to checkout the same branch 
# name for each submodule.

push=0
tagname=

function error_occurred {
    echo "Stopped because of error"
    exit
}

while getopts ":pt:" opt; do
  case $opt in
    p)
      push=1
      echo "Setting push"
      ;;
    t)
        tagname=$OPTARG
    ;;
    \?)
      echo "Usage: tag-all-repos.sh [-p] [-t  <tagname>]"
      exit 1
      ;;
  esac
done

if [[ -z "${tagname// }" ]]
then
    echo "Missing tag name"
    echo "Usage: tag-all-repos.sh [-p] [-t <tagname>]"
    exit 1
fi

if GIT_DIR=./.git git tag --list | egrep -q "^$1$"
then
    echo "Tag already exists."
    exit 0
fi

echo Tagging "$tagname"
git submodule foreach --recursive "git tag $tagname"
git tag "$tagname" || error_occurred

if [ "$push" -gt "0" ]
then
    echo Pushing "$tagname"
    git submodule foreach --recursive "git push --tags"
    git push --tags || error_occurred
fi