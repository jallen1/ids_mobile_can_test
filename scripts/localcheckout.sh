#!/bin/bash

set +v
# This is used to make sure all local repos at set to the same branch
#
# Checks out the main repo and all submodules on the given branch (LOCAL ONLY)
#

function error_occurred {
    echo "Stopped because of error"
    exit
}

if [ $# -eq 0 ];
then
    echo "Missing branch name"
    exit 1
fi

echo Checkout "$1"
git checkout "$1" || error_occurred
git submodule update || error_occurred
git submodule foreach --recursive "git checkout $1"
