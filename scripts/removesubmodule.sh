#!/bin/bash

set +v
# This script is used to remove a submodule from a git repository

function help {
    echo "Usage: removesubmodule.sh submodule"
    exit 1
}

if [ $# -eq 0 ];
then
    help
fi

submodule="$1"

git rm --cached "$submodule"
rm -rf .git/module/"$submodule"
git commit -m "Removed submodule $submodule"
