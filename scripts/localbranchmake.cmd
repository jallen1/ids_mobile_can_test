@echo off
rem This is used to make a local branch on all submodules.
rem

if "%1"=="" goto help

echo Make and Switch to Branch "%1"
git checkout -b %1
git submodule foreach --recursive git checkout -b %1

goto exit

:error
echo Stopped because of error
goto exit

:help
echo Missing branch name
goto exit

:exit