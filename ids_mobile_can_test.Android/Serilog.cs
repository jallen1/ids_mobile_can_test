﻿using System;
using Serilog;

namespace ids_mobile_can_test.Droid
{
    public static class Serilog
    {
        public static void Setup()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine($"Output log location: {SerilogConstants.LogFileNameFull}");
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.ControlledBy(SerilogConstants.LogLevelCurrent)
                    // The File sink has an option to support multi-threaded writes (shared).  However, this option doesn't seem to work correctly on iOS (no log file is
                    // actually generated).  So we use the Async sink to do all file writes on a single separate thread.  This allows us to not need the shared option given
                    // all writes will occur on a single Async thread.
                    //
                    // Moved NSLOG inside the Async as the NSLog Serilog sink wasn't writing message atomically.  Threads were stomping on each others log output. 
                    //
                    .WriteTo.Async(a => {
                        a.AndroidLog(outputTemplate: SerilogConstants.LogcatConsoleOutputTemplate);
                        a.File(SerilogConstants.LogFileNameFull, fileSizeLimitBytes: SerilogConstants.LogFileSizeLimitBytes, rollingInterval: RollingInterval.Day, retainedFileCountLimit: SerilogConstants.LogFileRetainedCountLimit, outputTemplate: SerilogConstants.LogFileOutputTemplate);
                    })
                    .CreateLogger();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"Unable to configure Serilog: {ex.Message}\n{ex.StackTrace}");
            }
        }
    }
}